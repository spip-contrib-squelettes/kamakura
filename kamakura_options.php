<?php
/**
 * Chargement du plugin Kamakura
 *
 * @plugin     Kamakura
 * @copyright  2021-2025
 * @author     erational
 * @licence    GNU/GPL
 * @package    SPIP\Kamakura\Options
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS['debut_intertitre'] = "\n<h2 class=\"spip\"><i class='fas fa-caret-square-right'></i>\n";
$GLOBALS['fin_intertitre'] = "\n</h2>\n";


// pagination : 6 liens max
define('_PAGINATION_NOMBRE_LIENS_MAX', 7);