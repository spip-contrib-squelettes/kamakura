<?php
/**
 * Fichier gérant l'installation et désinstallation du plugin Kamakura
 *
 * @plugin     Kamakura
 * @copyright  2020
 * @author     erational
 * @licence    GNU/GPL
 * @package    SPIP\Kamakura\Installation
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/meta');
include_spip('inc/cextras');
include_spip('base/kamakura');


/**
 * Fonction d'installation et de mise à jour du plugin Kamakura.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible
 *     Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 * @return void
**/
function kamakura_upgrade($nom_meta_base_version, $version_cible) {
	$maj = array();

	cextras_api_upgrade(kamakura_declarer_champs_extras(), $maj['create']);

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}


/**
 * Fonction de désinstallation du plugin Kamakura
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @return void
**/
function kamakura_vider_tables($nom_meta_base_version) {
	cextras_api_vider_tables(kamakura_declarer_champs_extras());
	effacer_config('kamakura');
	effacer_meta($nom_meta_base_version);
}

