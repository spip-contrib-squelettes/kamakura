<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

return [

//
// spip-core
//
'accueil_site' => 'Home',

// 0-9
'404_sorry' => 'There\'s nothing here.',
'404_comment' => 'Whatever you were looking for doesn\'t currently exist at this address. Unless you were looking for this error page, in which case: Congrats! You totally found it.',

// A
'actu' => 'Most recent',
'auteurs_les' => 'The authors',
'articles_tous' => 'All articles',

// C
'cfg_home_outro' => 'General presentation article',
'cfg_home_outro_explication' => 'Article at the bottom of the home page',
'cfg_home_rubrique_outro' => 'Focus section',
'cfg_home_rubrique_outro_explication' => 'Section whose last article is in focus',
'cfg_article_contact' => 'Contact article',
'cfg_homepage' => 'Home page',
'cfg_footer' => 'Pied de page',
'cfg_menu' => 'Footer',
'cfg_afficher_moteur_recherche' => 'Search engine',
'cfg_afficher_moteur_recherche_case' => 'Show search engine',
'cfg_afficher_accueil' => 'Menu',
'cfg_afficher_accueil_case' => 'Display the "home" link',
'cfg_footer_adresse' => 'Text in the central column (address, copyright, ...)',
'cfg_titre_parametrages' => 'Choose the content to display',
'cfg_footer_articles_contact' => 'Contact',
'cfg_footer_articles_contact_explication' => 'Article used to contact you',
'cfg_footer_articles' => 'Links',
'cfg_footer_articles_explication' => '(Optional) Articles to place as links. It is advisable not to exceed 2 or 3 links with short titles',
'cfg_intro' => 'This page allows you to customize your Kamakura skeleton',

'cfg_lien_doc' => 'Online Documentation',
'cfg_page_demo' => 'The skeleton comes with a demo page that allows you to test the layout with dummy content:',
'cfg_page_demo_article' => 'Demo article',

// E
'en_savoir_plus' => 'Learn more',

// L
'lire_la_suite' => 'Read more',


// M
'menu' => 'Menu',


// O
'ours' => 'Did you know that? <br /> The name of the skeleton <strong> Kamakura </strong> (鎌倉 市) comes from a seaside resort near Tokyo famous for its beach and the big Buddha. It is also the city where the book <i> The Tsubaki Stationery </i> by Ito Ogawa takes place.',


// P
'publie_le' => 'Published on',
'par' => 'by',
'pagination_pages' => 'Pages',
'pagination_gd_total' => 'available articles',
'posted' => 'Posted on',
'posted' => 'Last modification of this page',


// R
'resultats_out' => 'Available result(s)',
'recherche_site' => 'Result on:',
'recherche_recherche' => 'Search inside the website',
'recherche_nomatch' => 'Sorry, no results available for this search! ',
'resultats_articles' => 'Search on articles',
'recherche_resultat' => 'Research results on',
'recherche_titre' => 'Search',

// S
'kamakura_type_rubrique' => 'Section type',
'kamakura_type_rubrique_tri_date' => 'Articles listed by date (recent articles first)',
'kamakura_type_rubrique_tri_num' => 'Articles listed by number (10. xxx, 20. yyy, ...)',


// T
'top' => 'Top of page',
'titre_page_configurer_kamakura' => 'Configure Kamakura',


// V
'voir_aussi' => 'See as well',
'videos_plus' => 'More articles',


// X
'xxx' => '',


];
